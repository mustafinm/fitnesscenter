package mustafin.fintesscenter.api;

import java.util.Date;
import java.util.List;

import mustafin.fintesscenter.model.Schedule;
import mustafin.fintesscenter.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by mustafin on 2017-04-08 at 16:29.
 */

public interface FitnessApi {

    @POST("/users")
    Call<Void> postUser(@Body Request<User> req);

    @GET("/schedules")
    Call<List<Schedule>> schedules(@Body Request<Date> req);

    class Request<T>{
        String token;
        String number;
        T data;
    }

}
