package mustafin.fintesscenter.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by mustafin on 2017-04-08 at 14:55.
 */

public class Consts {

    private Consts(){}

    public static final Locale LOCALE = new Locale("ru");
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("H:mm", LOCALE);
    public static final DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", LOCALE);

}
