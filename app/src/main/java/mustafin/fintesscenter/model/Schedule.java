package mustafin.fintesscenter.model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by mustafin on 2017-04-08 at 13:56.
 */

public class Schedule extends SugarRecord{

    public Date dateStart;
    public Date dateEnd;
    public Coach coach;
    public Excercise excercise;
    public boolean isSelected;

    public Schedule() {
    }

    public Schedule(Date dateStart, Date dateEnd, Coach coach, Excercise excercise) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.coach = coach;
        this.excercise = excercise;
    }
}
