package mustafin.fintesscenter.model;

import com.orm.SugarRecord;

/**
 * Created by mustafin on 2017-04-08 at 13:51.
 */

public class Coach extends SugarRecord{

    public String firstName;
    public String lastName;
    public Excercise excercise;

    public Coach() {
    }

    public Coach(String firstName, String lastName, Excercise excercise) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.excercise = excercise;
    }
}
