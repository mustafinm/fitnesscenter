package mustafin.fintesscenter.model;

import com.orm.SugarRecord;

/**
 * Created by mustafin on 2017-04-08 at 13:54.
 */

public class Excercise extends SugarRecord{

    public String name;

    public Excercise() {
    }

    public Excercise(String name) {
        this.name = name;
    }
}
