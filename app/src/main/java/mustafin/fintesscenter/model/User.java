package mustafin.fintesscenter.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by mustafin on 2017-04-08 at 13:52.
 */

public class User extends SugarRecord {

    public String firstName;
    public String lastName;
    public List<Schedule> scheduleList;

    public User() {
    }

    public User(String firstName, String lastName, List<Schedule> scheduleList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.scheduleList = scheduleList;
    }
}
