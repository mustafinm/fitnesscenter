package mustafin.fintesscenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mustafin.fintesscenter.model.Coach;
import mustafin.fintesscenter.model.Excercise;
import mustafin.fintesscenter.model.Schedule;
import mustafin.fintesscenter.model.User;
import mustafin.fintesscenter.util.Consts;

/**
 * Created by mustafin on 2017-04-08 at 16:04.
 */

public class DummyData {

    public static void inserDummy(){

        deleteAll();
        if(Excercise.count(Excercise.class) == 0
                && Schedule.count(Excercise.class) == 0) {
            Excercise yoga = new Excercise("Йога");
            Excercise aerobic = new Excercise("Аэробика");
            Excercise crossfit = new Excercise("Кроссфит");
            Excercise sillovaya = new Excercise("Силовая трен.");
            yoga.save();
            aerobic.save();
            crossfit.save();
            sillovaya.save();

            Coach murat = new Coach("Мурат", "Мустафин", yoga);
            Coach almas = new Coach("Алмас", "Усенов", aerobic);
            Coach nurlan = new Coach("Нурлан", "Бекендов", crossfit);
            Coach valentin = new Coach("Валентин", "Фронтендов", sillovaya);
            murat.save();
            almas.save();
            nurlan.save();
            valentin.save();
            try {
                Date from = Consts.utcFormat.parse("2017-04-08T16:00:00Z");
                Date to = Consts.utcFormat.parse("2017-04-08T18:00:00Z");
                Schedule yogaSchedule = new Schedule(from, to, murat, yoga);
                Schedule aerobicSchedule = new Schedule(from, to, almas, aerobic);
                Schedule crossfitSchedule = new Schedule(from, to, nurlan, crossfit);
                Schedule sillovayaSchedule = new Schedule(from, to, valentin, sillovaya);
                yogaSchedule.save();
                aerobicSchedule.save();
                crossfitSchedule.save();
                sillovayaSchedule.save();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (User.count(User.class) != 0) {
            User user = new User("Вася", "Пупкин", null);
            user.save();
        }


    }

    public static void deleteAll(){
        Excercise.deleteAll(Excercise.class);
        User.deleteAll(User.class);
        Coach.deleteAll(Coach.class);
        Schedule.deleteAll(Schedule.class);
    }

}
