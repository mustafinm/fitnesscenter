package mustafin.fintesscenter.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mustafin.fintesscenter.R;
import mustafin.fintesscenter.model.Schedule;
import mustafin.fintesscenter.util.Consts;

/**
 * Created by mustafin on 2017-04-08 at 13:47.
 */

public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> {

    private List<Schedule> dataSet;
    private Context context;

    public ScheduleListAdapter(List<Schedule> dataSet, Context context) {
        this.dataSet = dataSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Schedule schedule = dataSet.get(position);
        if(schedule.excercise != null)
            holder.excercise.setText(schedule.excercise.name);
        if(schedule.coach != null)
            holder.coachName.setText(schedule.coach.firstName + " " + schedule.coach.lastName);

        if(schedule.dateStart != null && schedule.dateEnd != null) {
            String startTime = Consts.timeFormat.format(schedule.dateStart);
            String endTime = Consts.timeFormat.format(schedule.dateEnd);
            holder.time.setText(startTime + " - " + endTime);
        }

        if(schedule.isSelected){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.green));
        }else{
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        holder.itemView.setOnClickListener(v -> {
            selectSchedule(schedule);
        });
    }

    private void selectSchedule(Schedule schedule) {
        for (Schedule sc : dataSet) {
            if(sc.isSelected) {
                sc.isSelected = false;
                sc.save();
            }
        }
        schedule.isSelected = true;
        schedule.save();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView coachName;
        public TextView excercise;
        public TextView time;

        public ViewHolder(View view) {
            super(view);
            coachName = (TextView) view.findViewById(R.id.coach);
            excercise = (TextView) view.findViewById(R.id.excersise);
            time = (TextView) view.findViewById(R.id.time);
        }
    }
}
