package mustafin.fintesscenter.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;

import mustafin.fintesscenter.R;

public class CalendarActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        CalendarView calendarView = (CalendarView) findViewById(R.id.calendar);
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {

            Log.i("Date", year+" "+month+" "+dayOfMonth);
            Bundle bundle = new Bundle();
            bundle.putInt("year", year);
            bundle.putInt("month", month);
            bundle.putInt("dayOfMonth", dayOfMonth);

            startActivity(ScheduleListActivity.class, bundle);
        });

    }
}
