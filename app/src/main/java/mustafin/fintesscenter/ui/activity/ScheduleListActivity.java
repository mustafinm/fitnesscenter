package mustafin.fintesscenter.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.orm.SugarContext;

import java.util.List;

import icepick.Icepick;
import icepick.State;
import mustafin.fintesscenter.R;
import mustafin.fintesscenter.model.Schedule;
import mustafin.fintesscenter.ui.adapters.ScheduleListAdapter;

public class ScheduleListActivity extends AppCompatActivity {

    @State int year;
    @State int month;
    @State int dayOfMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SugarContext.init(this);

        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(R.layout.activity_schedule_list);
        retrieveBundle(getIntent().getExtras());

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        List<Schedule> schedules = Schedule.find(Schedule.class, null);
        recyclerView.setAdapter(new ScheduleListAdapter(schedules, getBaseContext()));

    }

    private void retrieveBundle(Bundle bundle){
        year = bundle.getInt("year");
        month = bundle.getInt("month");
        dayOfMonth = bundle.getInt("dayOfMonth");
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}
