package mustafin.fintesscenter.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.annotation.NonNull;


/**
 * Created by mustafin on 2017-04-08 at 13:14.
 */

public class BaseActivity extends AppCompatActivity {

    protected void startActivity(@NonNull Class<?> cls){
        startActivity(new Intent(getBaseContext(), cls));
    }

    protected void startActivity(@NonNull Class<?> cls, @NonNull Bundle bundle){
        Intent intent = new Intent(getBaseContext(), cls);
        intent.putExtras(bundle);
        startActivity(intent);
    }



}
