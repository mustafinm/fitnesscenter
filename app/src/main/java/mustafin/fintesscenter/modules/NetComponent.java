package mustafin.fintesscenter.modules;

import javax.inject.Singleton;

import dagger.Component;
import mustafin.fintesscenter.ui.activity.ScheduleListActivity;

/**
 * Created by mustafin on 2017-04-08 at 14:30.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetComponent {

    void inject(ScheduleListActivity activity);

}
